capturebtns()

function capturebtns() {

    document.addEventListener('keydown', (event) => {
        const key = event.key;
      
        switch (key) {
          case '.':
            document.querySelector('#btn_ponto').click();
            break;
      
          case 'Backspace':
            document.querySelector('#btn_Backspace').click();
            break;
      
          case '+':
            document.querySelector('#btn_sum').click();
            break;
      
          case '-':
            document.querySelector('#btn_sub').click();
            break;
      
          case '/':
            document.querySelector('#btn_div').click();
            break;
      
          case '*':
            document.querySelector('#btn_mult').click();
            break;
      
          case '=':
          case 'Enter':
            document.querySelector('#btn_equal').click();
            break;
      
          case ' ':
            document.querySelector('#btn_AC').click();
            break;
        }
      
        if (!isNaN(key) && key !== '*') {
          document.querySelector(`#btn_${key}`).click();
        }
    });
      
}

function mascara(monitor, key) {


    if (monitor == '0') {
        if (key == '.') {
            return '0.';
        } else {
            return key;
        }

    } else {

        let numero = monitor + key

        // Conta o número de ocorrências do caractere '.'
        const countDots = numero.split('.').length - 1;
        let firstDotIndex = numero.indexOf('.');

        // Se há mais de um ponto, remove os pontos extras
        if (countDots > 1) {
            const lastDotIndex = numero.lastIndexOf('.');
            if (firstDotIndex !== lastDotIndex) {
                numero = numero.slice(0, lastDotIndex) + numero.slice(lastDotIndex + 1);
            }
        }

        return numero;

    }


}

function atributos(monitor, conta, res, btn) {
    //Para numeros e o ponto
    if (!isNaN(btn) || btn == '.') {

        if (monitor == '0') {
            if (btn == '.') {
                monitor = '0.';
            } else {
                monitor = btn;
            }


        } else {

            let numero = '';

            /*
                Res é respnsavel por verificar se o valor que está no monitor
                é a resposta ou o numero digitado pelo user.
                Se for a resposta então ao digitar ele vai substituir o valor
                Se não encrementa normalmente
            */
            if (res) {
                numero = btn
                res = false
            } else {
                numero = monitor + btn
            }


            // Conta o número de ocorrências do caractere '.'
            const countDots = numero.split('.').length - 1;
            let firstDotIndex = numero.indexOf('.');

            // Se há mais de um ponto, remove os pontos extras
            if (countDots > 1) {
                const lastDotIndex = numero.lastIndexOf('.');
                if (firstDotIndex !== lastDotIndex) {
                    numero = numero.slice(0, lastDotIndex) + numero.slice(lastDotIndex + 1);
                }
            }
            monitor = numero;

        }
    }

    //Operaçoes aritimeticas
    if (btn == '+' || btn == '-' || btn == '/' || btn == 'x') {

        if (conta.indexOf("=") != -1) {

            conta = monitor + ' ' + btn;

        } else if (conta != '') {

            conta = conta + ' ' + monitor;
            monitor = eval(conta.replace('x', '*')).toFixed(4).replace(/\.?0+$/, '');
            conta = monitor + ' ' + btn;


        } else {
           
            conta = monitor + ' ' + btn;
            
        }
        res = true;
    }

    if (btn == '=' && conta.indexOf("=") == -1) {
        if (Math.sign(monitor) == -1) {
            //Se  o o numero for negativo fica em parenteses
            conta = conta+' (' + monitor + ') ';
        } else {
            conta = conta + ' ' + monitor;
        }
        monitor = eval(conta.replace('x', '*')).toFixed(4).replace(/\.?0+$/, '');
        conta = conta + ' ' + btn;

        res = true;
    }

    if (btn == 'AC') {
        monitor = 0
        conta = ''
        res = false
    }

    if (btn == 'backspace') {
        if (res) {
            conta = '';
        } else {
            monitor = monitor.substring(0, monitor.length - 1);
        }


        if (monitor == '') {
            monitor = '0';
        }
    }

    if (btn == '+/-') {
        monitor = monitor * -1;
    }
    if (btn == '%') {
        monitor = monitor / 100;
    }


    return {
        'monitor': monitor,
        'conta': conta,
        'res': res
    }
}
