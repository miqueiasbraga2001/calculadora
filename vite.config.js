import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import sass from 'sass'; // importe o pacote sass

export default defineConfig({
  plugins: [
    laravel({
      input: ['resources/css/app.css', 'resources/js/app.js'],
      refresh: true,
    }),
  ],
  css: {
    preprocessorOptions: {
      sass: {
        additionalData: '@import "node_modules/bootstrap/scss/functions"; @import "node_modules/bootstrap/scss/variables"; @import "node_modules/bootswatch/dist/cyborg/_variables.scss";',
        implementation: sass,
      },
    },
  },
});
