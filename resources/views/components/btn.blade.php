<div x-data="" class="col px-1">
 
    <div class="d-sm-block d-none">
        <button {{ $attributes->merge(['class' => 'btn btn-'.$type.' w-100']) }} 
        @click="dados = atributos( dados.monitor, dados.conta, dados.res, '{{$value}}' )">

            {{--No btn de apagar ele irá mostrar o icone de backspace e não o nome--}}
            @if($value != 'backspace')
            {{$value}}
            @else
            {{$slot}}
            @endif

        </button>
    </div>

    <div class="d-sm-none d-block">
        <button {{ $attributes->merge(['class' => 'btn btn-sm btn-'.$type.' w-100']) }} 
            @click="dados = atributos( dados.monitor, dados.conta, dados.res, '{{$value}}' )">
    
        {{--No btn de apagar ele irá mostrar o icone de backspace e não o nome--}}
        @if($value != 'backspace')
            {{$value}}
        @else
            {{$slot}}
        @endif
    
        </button>
    
      </div>

    
</div>
