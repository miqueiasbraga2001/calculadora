<div x-data="">
    <div class="form-group" >
     
        <fieldset style="overflow: hidden">
          <div class="form-control form-control-lg pb-2 pt-1 text-end fs-2">

            <span class="fs-5" x-text="dados.conta"></span>
            <br>
            <span class="display-4" x-text="dados.monitor"></span>
            
          </div>
        </fieldset>
      </div>
</div>