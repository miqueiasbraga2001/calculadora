<div class="row row-cols-4 gx-3 gy-4">

    <x-btn id="btn_AC" type='outline-primary' value="AC"></x-btn>
    <x-btn id="btn_negate" type='outline-dark' value="+/-"></x-btn>
    <x-btn id="btn_%" type='outline-dark' value="%"></x-btn>
    <x-btn id="btn_div" type='outline-dark' value="/"></x-btn>

    <x-btn id="btn_7" type='light' value="7"></x-btn>
    <x-btn id="btn_8" type='light' value="8"></x-btn>
    <x-btn id="btn_9" type='light' value="9"></x-btn>
    <x-btn id="btn_mult" type='outline-dark' value="x"></x-btn>

    <x-btn id="btn_4" type='light' value="4"></x-btn>
    <x-btn id="btn_5" type='light' value="5"></x-btn>
    <x-btn id="btn_6" type='light' value="6"></x-btn>
    <x-btn id="btn_sub" type='outline-dark' value="-"></x-btn>

    <x-btn id="btn_1" type='light' value="1"></x-btn>
    <x-btn id="btn_2" type='light' value="2"></x-btn>
    <x-btn id="btn_3" type='light' value="3"></x-btn>
    <x-btn id="btn_sum" type='outline-dark' value="+"></x-btn>

    <x-btn id="btn_0" type='light' value="0"></x-btn>
    <x-btn id="btn_ponto" type='light' value="."></x-btn>
    <x-btn id="btn_Backspace" type='danger' class="" value="backspace">
        <i class="bi bi-backspace-fill"></i>
    </x-btn>
    <x-btn id="btn_equal" type='outline-warning' value="="></x-btn>
</div>

<script>
  
</script>
