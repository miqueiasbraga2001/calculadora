@extends('layouts.main')

@section('titulo', 'Calculadora')

@section('conteudo')
    
<div class="row m-auto vh-100">
    <x-calculadora></x-calculadora>
</div>

@endsection